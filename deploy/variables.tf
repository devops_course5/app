variable "prefix" {
  default = "app"
}

variable "project" {
  default = "app"
}

variable "contact" {
  default = "rengler33@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS Postgres instance"
}

variable "bastion_key_name" {
  # to match the name of the ssh key pair added to ec2
  default = "app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  # CI overrides this but this allows us to deploy from local machine
  default = "605660357463.dkr.ecr.us-east-1.amazonaws.com/app:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  # CI overrides this but this allows us to deploy from local machine
  default = "605660357463.dkr.ecr.us-east-1.amazonaws.com/app-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for django app"
}
