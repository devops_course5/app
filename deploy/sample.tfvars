# create a file called terraform.tfvars to supply these values
# these will be picked up when running terraform locally
db_username       = "app"
db_password       = "changeme"
django_secret_key = "changeme"
