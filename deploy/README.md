# Deploy

The purpose of the `docker-compose.yml` file in this folder is to standardize the version of terraform used for everyone using this project. Since running a newer version of terraform could update the state file to a newer version and introduce breaking changes for everyone else, it's important that the terraform version be standardized.
